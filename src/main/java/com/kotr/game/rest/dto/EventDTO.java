package com.kotr.game.rest.dto;

import com.kotr.game.domain.GameEngine;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class EventDTO {
    String eventType;
    List<PlayerDTO> players;

    String message;

    public static EventDTO toDto(GameEngine.Event event) {
        EventDTO dto = new EventDTO();

        dto.eventType = event.getType().name();
        dto.players = event.getPlayers()
                .stream()
                .map(PlayerDTO::toDto)
                .collect(Collectors.toList());

        dto.setMessage(event.getText());

        return dto;
    }
}
