package com.kotr.game.rest.dto;

import com.kotr.game.domain.model.weapons.Weapon;
import lombok.Data;

@Data
public class WeaponDTO {
    private String name;
    private int minDamage;
    private int maxDamage;
    private String verb;

    public static WeaponDTO toDto(Weapon weapon) {
        WeaponDTO dto = new WeaponDTO();

        dto.name = weapon.getName();
        dto.minDamage = weapon.getMinDamage();
        dto.maxDamage = weapon.getMaxDamage();
        dto.verb = weapon.getVerb();

        return dto;
    }
}
