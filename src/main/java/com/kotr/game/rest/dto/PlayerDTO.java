package com.kotr.game.rest.dto;

import com.kotr.game.domain.model.player.Player;
import lombok.Data;

@Data
public class PlayerDTO {
    private String name;
    private int health;
    private int chanceToHit;
    private int chanceToDodge;
    private WeaponDTO weapon;

    public static PlayerDTO toDto(Player player) {
        PlayerDTO dto = new PlayerDTO();

        dto.name = player.getName();
        dto.health = player.getHealth();
        dto.chanceToHit = player.getChanceToHit();
        dto.chanceToDodge = player.getChanceToDodge();
        dto.weapon = WeaponDTO.toDto(player.getWeapon());

        return dto;
    }
}
