package com.kotr.game.rest.dto;

import com.kotr.game.domain.model.team.Team;
import lombok.Data;

@Data
public class TeamDTO {
    String teamName;

    public static TeamDTO toDto(Team team) {
        TeamDTO dto = new TeamDTO();

        dto.teamName = team.getTeamName();

        return dto;
    }
}
