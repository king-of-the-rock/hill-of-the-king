package com.kotr.game.rest;

import com.kotr.game.config.Beans;
import com.kotr.game.domain.GameEngine;
import com.kotr.game.domain.model.player.Player;
import com.kotr.game.domain.model.team.Team;
import com.kotr.game.domain.model.weapons.Hammer;
import com.kotr.game.domain.model.weapons.Knife;


import com.kotr.game.rest.dto.EventDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class GameController {
	GameEngine engine = Beans.gameEngine();

	@GetMapping("start_simulation")
	public List<EventDTO> startSimulation() {

		var player1 = new Player();
		var player2 = new Player();

		player1.equip(new Hammer());
		player2.equip(new Knife());

		engine.startPlayerSimulation(player2, player1);

		return engine.getEvents()
				.stream()
				.map(EventDTO::toDto)
				.collect(Collectors.toList());
	}

	@GetMapping("start_team_simulation")
	public List<EventDTO> startTeamSimulation() {

		var teamRed = new Team("Red", List.of(new Player(), new Player()));
		var teamBlue = new Team("Blue", List.of(new Player(), new Player()));

		teamRed.getPlayersInTeam().forEach(player -> player.equip(new Knife()));

		teamBlue.getPlayersInTeam().forEach(player -> player.equip(new Hammer()));

		engine.startTeamSimulation(teamRed, teamBlue);

		return engine.getEvents()
				.stream()
				.map(EventDTO::toDto)
				.collect(Collectors.toList());
	}

}
