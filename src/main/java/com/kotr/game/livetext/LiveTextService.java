package com.kotr.game.livetext;

import com.kotr.game.domain.GameEngine;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import static java.text.MessageFormat.format;


@Service
@RequiredArgsConstructor
public class LiveTextService {

	private final LanguageTexts texts;

	public String getMessageForEvent(GameEngine.Event event) {
		switch (event.getType()) {
			case EQUIP_WEAPON: return format(texts.getEquipWeapon(),
					event.getMainPlayer().getName(),
					event.getMainPlayer().getWeapon().getName());

			case START_ATTACK: return format(texts.getStartAttack(),
					event.getMainPlayer().getName(),
					event.getMainPlayer().getWeapon().getVerb(),
					event.getPlayers().get(1).getName());

			case TRY_DEFEND: return format(texts.getTryDefend(),
					event.getMainPlayer().getName());

			case DODGE_ATTACK: return format(texts.getDodgeAttack(),
					event.getMainPlayer().getName());

			case DEAD: return format(texts.getDied(),
					event.getMainPlayer().getName());

			case MISS_ATTACK: return format(texts.getMissesAttack(),
					event.getMainPlayer().getName(),
					event.getPlayers().get(1).getName());

			case TAKE_DAMAGE: return format(texts.getTakeDamage(),
					event.getMainPlayer().getName(),
					event.getMainPlayer().getWeapon().getCurrentDamage(),
					event.getMainPlayer().getHealth());
		}
		return Strings.EMPTY;
	}

}
