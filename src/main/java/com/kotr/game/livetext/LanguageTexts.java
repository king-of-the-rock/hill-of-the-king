package com.kotr.game.livetext;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class LanguageTexts {
	//type
	@Value("${story.arena-description.type.desert}")
	public String desert;

	@Value("${story.arena-description.type.forest}")
	public String forest;

	//weather
	@Value("${story.arena-description.weather.sunny}")
	public String sunny;

	@Value("${story.arena-description.weather.rainy}")
	public String rainy;

	@Value("${story.equip-weapon}")
	public String equipWeapon;

	@Value("${story.start-attack}")
	private String startAttack;

	@Value("${story.misses-attack}")
	private String missesAttack;

	@Value("${story.try-defend}")
	private String tryDefend;

	@Value("${story.dodge-attack}")
	private String dodgeAttack;

	@Value("${story.deal-damage}")
	private String takeDamage;

	@Value("${story.died}")
	private String died;
}
