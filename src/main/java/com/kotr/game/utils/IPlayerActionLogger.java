package com.kotr.game.utils;

@Deprecated
public interface IPlayerActionLogger {
    void equipWeapon(String player, String weapon);
    void prepareAttack(String attacker, String weaponVerb, String defender);
    void tryAttack(String attacker, String defender);
    void dodgeAttack(String defender);
    void takeDamage(String attacker, int damageTaken, int health);
    void playerDied(String player);
}
