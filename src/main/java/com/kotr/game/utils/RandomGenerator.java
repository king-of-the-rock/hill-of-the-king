package com.kotr.game.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Random;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RandomGenerator {

	public static final RandomGenerator instance = new RandomGenerator();

	public static RandomGenerator getInstance() {
		return instance;
	}

	private Random random = new Random();


	public void setSeed(long seed) {
		random.setSeed(seed);
	}

//	public void reset() {
//		random = new Random(100L);
//	}

	public int nextInt(int from, int to) {
		return random.ints(from, to).findFirst().getAsInt();
	}

	public int nextPercentage() {
		return random.ints(0, 100).findFirst().getAsInt();
	}

	public boolean isSuccessful(int chance) {
		return nextPercentage() < chance;
	}

	public boolean isNotSuccessful(int chance) {
		return nextPercentage() >= chance;
	}

	public String nextString(String[] string) {
		return string[nextInt(0, string.length)];
	}
}
