package com.kotr.game.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Deprecated
public class PlayerActionLogger implements IPlayerActionLogger {

    @Override
    public void equipWeapon(String player, String weapon) {
        log.info("'{}' equipped a {}.", player, weapon);
    }

    public void prepareAttack(String attacker, String weaponVerb, String defender) {
        log.info("'{}' is trying to {} '{}'", attacker, weaponVerb, defender);
    }

    @Override
    public void tryAttack(String attacker, String defender) {
        log.info("'{}' misses the hit.. unlucky!! Merde!! '{}' is charging forward!", attacker, defender);
    }

    @Override
    public void dodgeAttack(String defender) {
        log.info("'{}' quickly shifts back and dodges the attack laughing.", defender);
    }

    @Override
    public void takeDamage(String defender, int damageTaken, int health) {
        log.info("'{}' took *{}* damage and now has {} health.", defender, damageTaken, health);
    }

    @Override
    public void playerDied(String player) {
        log.info("'{}' died!", player);
    }
}
