package com.kotr.game.domain.model.weapons;

public class Knife extends Weapon {
	private static final String NAME = "Wicked Knife";
	private static final int MIN_DAMAGE = 10;
	private static final int MAX_DAMAGE = 15;
	private static final String VERB = "stab";

	public Knife() {
		super(NAME, MIN_DAMAGE, MAX_DAMAGE);
		this.verb = VERB;
	}

}
