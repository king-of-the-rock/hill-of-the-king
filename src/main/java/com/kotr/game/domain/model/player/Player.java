package com.kotr.game.domain.model.player;

import com.kotr.game.domain.model.GeneratePlayerStats;
import com.kotr.game.domain.model.player.actions.AttackAction;
import com.kotr.game.domain.model.player.actions.DefendAction;
import com.kotr.game.domain.model.weapons.Fists;
import com.kotr.game.domain.model.weapons.Weapon;
import lombok.*;
import org.apache.commons.lang.SerializationUtils;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class Player implements Serializable {

	private GeneratePlayerStats playerStats = new GeneratePlayerStats();
	private final String name = playerStats.getName();
	private int health = playerStats.getHealth();
	private int chanceToHit = playerStats.getChanceToHit();
	private int chanceToDodge = playerStats.getChanceToDodge();

	@Setter(AccessLevel.NONE)
	private Weapon weapon = new Fists();

	public void attack(Player otherPlayer) {
		new AttackAction(this).attack(otherPlayer);
	}

	public void defend(int potentialDamage) {
		new DefendAction(this).defend(potentialDamage);
	}

	public boolean isAlive() {
		return health > 0;
	}

	public boolean isDead() {
		return !isAlive();
	}

	public void equip(Weapon weapon) {
		this.weapon = weapon;
	}

	public Player clone() {
		return (Player) SerializationUtils.clone(this);
	}
}
