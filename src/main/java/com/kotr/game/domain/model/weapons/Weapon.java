package com.kotr.game.domain.model.weapons;

import com.kotr.game.utils.RandomGenerator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Getter
@RequiredArgsConstructor
public abstract class Weapon implements Serializable {

	private final String name;
	private final int minDamage;
	private final int maxDamage;
	private int currentDamage;

	private final transient RandomGenerator random = RandomGenerator.getInstance();
	protected String verb = "hit";

	public int getDamage() {
		currentDamage = random.nextInt(minDamage, maxDamage);
		return currentDamage;
	}
}
