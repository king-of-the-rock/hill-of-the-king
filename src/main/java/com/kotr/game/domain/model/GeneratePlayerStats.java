package com.kotr.game.domain.model;

import com.kotr.game.utils.RandomGenerator;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Data
@RequiredArgsConstructor
public class GeneratePlayerStats implements Serializable {

	@Getter(AccessLevel.NONE)
	private final transient RandomGenerator random = RandomGenerator.getInstance();
	private final String[] names = new String[]{"Daniel", "Emanuel", "Sam", "Santa Claus", "Grinch", "Horse", "Chicken"};

	private final String name = random.nextString(names);
	private final int health = random.nextInt(30, 100);
	private final int chanceToHit = random.nextInt(80, 90);
	private final int chanceToDodge = random.nextInt(10, 25);
}
