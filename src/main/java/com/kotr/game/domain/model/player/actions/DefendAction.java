package com.kotr.game.domain.model.player.actions;

import com.kotr.game.domain.events.EventManager;
import com.kotr.game.domain.events.PlayerEventType;
import com.kotr.game.domain.model.player.Player;
import com.kotr.game.utils.RandomGenerator;

import java.util.List;

public class DefendAction {
    private final Player thisPlayer;

    private final RandomGenerator random = RandomGenerator.getInstance();

    public DefendAction(Player thisPlayer) {
        this.thisPlayer = thisPlayer;
    }

    public void defend(int damageToTake) {

        if(random.isSuccessful(thisPlayer.getChanceToDodge())) {
            EventManager.getInstance().publish(PlayerEventType.DODGE_ATTACK, List.of(thisPlayer));
            return;
        }

        thisPlayer.setHealth(thisPlayer.getHealth() - damageToTake);

        EventManager.getInstance().publish(PlayerEventType.TAKE_DAMAGE, List.of(thisPlayer));

        if (thisPlayer.isDead()) {
            EventManager.getInstance().publish(PlayerEventType.DEAD, List.of(thisPlayer));
        }
    }
}
