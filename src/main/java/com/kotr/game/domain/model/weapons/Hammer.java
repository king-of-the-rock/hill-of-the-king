package com.kotr.game.domain.model.weapons;

public class Hammer extends Weapon {
	private static final String NAME = "Hammer";
	private static final int MIN_DAMAGE = 5;
	private static final int MAX_DAMAGE = 25;
	private static final String VERB = "bash";

	public Hammer() {
		super(NAME, MIN_DAMAGE, MAX_DAMAGE);
		this.verb = VERB;
	}

}
