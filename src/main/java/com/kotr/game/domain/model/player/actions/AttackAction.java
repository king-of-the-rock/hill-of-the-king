package com.kotr.game.domain.model.player.actions;

import com.kotr.game.domain.events.PlayerEventType;
import com.kotr.game.domain.events.EventManager;
import com.kotr.game.domain.model.player.Player;
import com.kotr.game.utils.RandomGenerator;

import java.util.List;

public class AttackAction {

    private final Player thisPlayer;

    EventManager eventManager = EventManager.getInstance();
    private final RandomGenerator random = RandomGenerator.getInstance();

    public AttackAction(Player thisPlayer) {
        this.thisPlayer = thisPlayer;
    }

    public void attack(Player otherPlayer) {
        if (thisPlayer.isDead()) {
            return;
        }

        if (random.isNotSuccessful(thisPlayer.getChanceToHit())) {
            eventManager.publish(PlayerEventType.MISS_ATTACK, List.of(thisPlayer, otherPlayer));
            return;
        }

        eventManager.publish(PlayerEventType.TRY_DEFEND, List.of(otherPlayer));
    }

}
