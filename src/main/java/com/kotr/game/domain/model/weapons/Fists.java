package com.kotr.game.domain.model.weapons;

public class Fists extends Weapon {
	private static final String NAME = "Fists";
	private static final int MIN_DAMAGE = 2;
	private static final int MAX_DAMAGE = 5;

	public Fists() {
		super(NAME, MIN_DAMAGE, MAX_DAMAGE);
	}

}
