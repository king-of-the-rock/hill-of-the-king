package com.kotr.game.domain.model.team;

import com.kotr.game.domain.model.player.Player;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.stream.Stream;

@Data
@RequiredArgsConstructor
public class Team {

    private final String teamName;
    private final List<Player> playersInTeam;

    private int currentPlayerToAttachIndex = 0;


    public Player getPlayerToDefend() {
        return getAlivePlayers().findAny().orElse(null);
    }

    public Player getPlayerToAttack() {
        return getAlivePlayers().skip(currentPlayerToAttachIndex).findFirst().orElse(null);
    }

    private Stream<Player> getAlivePlayers() {
        return playersInTeam.stream().filter(Player::isAlive);
    }

    public boolean isActive() {
        return getAlivePlayers().findAny().isPresent();
    }

}
