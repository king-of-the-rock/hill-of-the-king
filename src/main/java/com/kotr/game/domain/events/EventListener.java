package com.kotr.game.domain.events;

import com.kotr.game.domain.model.player.Player;

import java.util.List;

public interface EventListener {

    void update(PlayerEventType eventType, List<Player> players);
}
