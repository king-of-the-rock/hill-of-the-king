package com.kotr.game.domain.events;

public enum PlayerEventType {
	EQUIP_WEAPON,
	START_ATTACK,
	MISS_ATTACK,
	TRY_DEFEND,
	DODGE_ATTACK,
    TAKE_DAMAGE,
	DEAD
}
