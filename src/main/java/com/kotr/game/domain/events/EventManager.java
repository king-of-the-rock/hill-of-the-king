package com.kotr.game.domain.events;

import com.kotr.game.domain.model.player.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class EventManager {
    private static EventManager instance;
    Map<PlayerEventType, List<EventListener>> listeners = new HashMap<>();

    public static EventManager getInstance() {
        if (instance == null) {
            instance = new EventManager();
        }
        return instance;
    }

    public EventManager() {
        for (PlayerEventType action : PlayerEventType.values()) {
            this.listeners.put(action, new ArrayList<>());
        }
    }

    public void subscribe(PlayerEventType eventType, EventListener listener) {
        listeners.get(eventType).add(listener);
    }

    public void subscribe(List<PlayerEventType> eventTypes, EventListener listener) {
        eventTypes.forEach(eventType -> subscribe(eventType, listener));
    }

    public void unsubscribe(PlayerEventType eventType, EventListener listener) {
        listeners.get(eventType).remove(listener);
    }

    public void unsubscribe(List<PlayerEventType> eventTypes, EventListener listener) {
        eventTypes.forEach(eventType -> unsubscribe(eventType, listener));
    }

    public void publish(PlayerEventType eventType, List<Player> players) {
        listeners.get(eventType)
                .forEach(listeners -> listeners.update(eventType, players));
    }
}
