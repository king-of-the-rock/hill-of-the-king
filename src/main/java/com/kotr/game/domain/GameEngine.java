package com.kotr.game.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import com.kotr.game.domain.events.EventManager;
import com.kotr.game.domain.events.PlayerEventType;
import com.kotr.game.domain.model.player.Player;
import com.kotr.game.domain.model.team.Team;
import com.kotr.game.livetext.LiveTextService;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import java.util.stream.Collectors;

@Service
@RequestScope
//@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@RequiredArgsConstructor
public class GameEngine {

	@Autowired
	private LiveTextService liveTextService;

	@Getter
	private Set<Event> events = new LinkedHashSet<>();

	private final EventManager eventManager = EventManager.getInstance();

	@SneakyThrows
	public void startPlayerSimulation(Player p1, Player p2) {
		int i = 0;

		addListeners(events);
		addListeners(p1);
		addListeners(p2);

		eventManager.publish(PlayerEventType.EQUIP_WEAPON, List.of(p1));
		eventManager.publish(PlayerEventType.EQUIP_WEAPON, List.of(p2));

		while (p1.isAlive() && p2.isAlive()) {

			eventManager.publish(PlayerEventType.START_ATTACK, List.of(p1, p2));
			eventManager.publish(PlayerEventType.START_ATTACK, List.of(p2, p1));

			if (++i > 100) {
				throw new RuntimeException("more than 100 loops.");
			}
		}
	}

	@SneakyThrows
	public void startTeamSimulation(Team t1, Team t2) {
		int i = 0;

		addListeners(events);

		t1.getPlayersInTeam().forEach(player -> addListeners(player));
		t2.getPlayersInTeam().forEach(player -> addListeners(player));

		t1.getPlayersInTeam().forEach(playerT1 -> {
			eventManager.publish(PlayerEventType.EQUIP_WEAPON, List.of(playerT1));
		});

		t2.getPlayersInTeam().forEach(playerT2 -> {
			eventManager.publish(PlayerEventType.EQUIP_WEAPON, List.of(playerT2));
		});

		while (gameInProgress(t1, t2)) {


			for (int y = 0; y < t1.getPlayersInTeam().size(); y++) {

				if (gameInProgress(t1, t2)) {
					attackOtherTeam(t1, t2);
				}
				if (gameInProgress(t1, t2)) {
					attackOtherTeam(t2, t1);
				}
			}

			if (++i > 100) {
				throw new RuntimeException("more than 1000 loops.");
			}
		}
	}

	private boolean gameInProgress(Team t1, Team t2) {
		return t1.isActive() && t2.isActive();
	}

	private void attackOtherTeam(Team attackingTeam, Team defendingTeam) {
		eventManager.publish(PlayerEventType.START_ATTACK,
				List.of(
					attackingTeam.getPlayerToAttack(),
					defendingTeam.getPlayerToDefend()));
	}

	private void addListeners(Set<Event> events) {
		eventManager.subscribe(Arrays.asList(PlayerEventType.values()), (type, players) -> {
			Event event = new Event(type,
					players.stream().map(Player::clone).collect(Collectors.toList()));

			event.setText(liveTextService.getMessageForEvent(event));
			events.add(event);
		});
	}

	private void addListeners(Player player) {
		eventManager.subscribe(PlayerEventType.START_ATTACK, (eventType, players) -> {
			if (players.get(0).equals(player)) {
				player.attack(players.get(1));
			}
		});

		eventManager.subscribe(PlayerEventType.TRY_DEFEND, (eventType, players) -> {
			int weaponDamage = players.get(0).getWeapon().getDamage();

			if (players.get(0).equals(player)) {
				player.defend(weaponDamage);
			}
		});
	}

	@Data
	@NoArgsConstructor
	public static class Event {
		private PlayerEventType type;
		private List<Player> players = new ArrayList<>();

		private String text;

		public Event(PlayerEventType type, List<Player> players) {
			this.type = type;
			this.players = players;
		}

		public Player getMainPlayer() {
			return players.get(0);
		}
	}

}
