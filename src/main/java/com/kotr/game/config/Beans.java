package com.kotr.game.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.kotr.game.domain.GameEngine;

@Component
public class Beans implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static GameEngine gameEngine() {
        return applicationContext.getBean(GameEngine.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }
}
