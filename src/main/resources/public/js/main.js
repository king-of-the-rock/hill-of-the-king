
var kotr = {}

axios.get("/api/start_team_simulation")
    .then(response => {
        console.log(response)
        kotr.eventsDisplay = newEventsDisplayComponent(response.data)
    }).catch(error => {
        console.error(error);
        alert(error);
    });

function newEventsDisplayComponent(events) {
    var eventsTextComponent = newEventText(events[0].message);
    var eventsIndexBarComponent = newEventIndexBar(events.length);
    
    let component = {
        events: events,

        onNext: function() {
            eventsIndexBarComponent.goToNextEvent();
            this.refresh();
        },

        onPrevious: function() {
            eventsIndexBarComponent.goToPreviousEvent();
            this.refresh();
        },

        refresh: function() {
            var evt = events[eventsIndexBarComponent.index];
            eventsTextComponent.setText(evt);

            document.getElementById("evt-current-type").innerText = evt.eventType;
        }
    }
    component.refresh();
    return component;
}

function newEventText(initialText) {
    let eventText = {
        id: "evt-current-text",
    
        setText: function(event) {
            let newText = "";
            let players = event.players

            switch(event.eventType) {
                case "EQUIP_WEAPON":
                    let weapon = players[0].weapon;
                    newText = `${event.message} <br> <b>${players[0].name}</b> ${kotr.emojies.dagger} ${weapon.name} <i>(${weapon.minDamage}~${weapon.maxDamage} dmg)</i>`;
                    break;
                case "START_ATTACK":
                    newText = `${event.message} <br> (${kotr.emojies.heart}${players[0].health}) <b>${players[0].name}</b> (${players[0].weapon.name}) ${kotr.emojies.swords}  <b>${players[1].name}</b> (${kotr.emojies.heart}${players[1].health})`;
                    break;
                case "TAKE_DAMAGE":
                    newText = `${event.message} <br> <b>${players[0].name}</b> ${kotr.emojies.heartBroken} ${players[0].health}`;
                    break;
                default:
                    newText = (!!!event.message) ? "(no message)" : event.message;
            }
            document.getElementById(this.id).innerHTML = newText;
        }
    };
    eventText.setText(initialText);
    return eventText;
}


function newEventIndexBar(eventsCount) {
    let eventIndexBar = {
        id: "evt-current-index",
        index: 0,
        totalEvents: eventsCount,

        getPresentatonString: function () {
            return this.index + "/" + this.totalEvents
        },

        goToNextEvent: function () {
            if (this.index + 1 > this.totalEvents) {
                alert("cannot go to next event!")
                return;
            }
            this.setIndex(this.index + 1);
        },

        goToPreviousEvent: function () {
            if (this.index - 1 < 0) {
                alert("cannot go to next event!")
                return;
            }
            this.setIndex(this.index - 1);
        },

        setIndex: function (newIndex) {
            this.index = newIndex;
            document.getElementById(this.id).innerText = this.getPresentatonString();
        },

        reset: function () {
            this.setIndex(0);
        }
    };

    eventIndexBar.reset();
    return eventIndexBar;
}

kotr.emojies = {
    swords: "⚔",
    skull: "☠",
    dagger: "🗡",
    heart: "❤",
    heartBroken: "💔",
}