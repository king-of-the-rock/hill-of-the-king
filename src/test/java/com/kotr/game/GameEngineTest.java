package com.kotr.game;

import com.kotr.game.domain.GameEngine;
import com.kotr.game.domain.model.player.Player;
import com.kotr.game.domain.model.team.Team;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GameEngineTest {

	@Autowired
	private GameEngine gameEngine;

	@Test
	void test() {

		var teamRed = new Team("Red", List.of(new Player(), new Player()));
		var teamBlue = new Team("Blue", List.of(new Player(), new Player()));

		gameEngine.startTeamSimulation(teamRed, teamBlue);
	}

}