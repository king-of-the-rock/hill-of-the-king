package com.kotr.game.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;

public class TestUtils {

	private static ObjectWriter jsonSerializer = new ObjectMapper().writerWithDefaultPrettyPrinter();

	@SneakyThrows
	public static String toJson(Object object) {
		return jsonSerializer.writeValueAsString(object);
	}

}
