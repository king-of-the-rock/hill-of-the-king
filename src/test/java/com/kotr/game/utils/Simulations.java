package com.kotr.game.utils;

import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;

public class Simulations {

	@SneakyThrows
	public static String twoPlayersSeed100() {
		return Files.readString(Path.of("./src/test/resources/simulations/2player_seed100.json"));
	}

	@SneakyThrows
	public static String twoVTwoTeamSeed100() {
		return Files.readString(Path.of("./src/test/resources/simulations/2v2team_seed100.json"));
	}
}
