package com.kotr.game;

import com.kotr.game.rest.GameController;
import com.kotr.game.utils.RandomGenerator;
import com.kotr.game.utils.Simulations;
import lombok.SneakyThrows;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static com.kotr.game.utils.TestUtils.toJson;
import static org.junit.jupiter.api.Assertions.assertEquals;

//FIXME: Tests failing for multiple/different clients
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class BlackBoxTest {

	@Autowired
	private GameController controller;

	@BeforeEach
	void beforeEach() {
		RandomGenerator.getInstance().setSeed(100L);
	}

	@SneakyThrows
	@Test
	public void test() {
		//when
		var events = controller.startSimulation();
		var response = toJson(events);

		//then
		assertEquals(response, Simulations.twoPlayersSeed100());
	}

	@SneakyThrows
	@Test
	public void test1() {
		//when
		var events = controller.startTeamSimulation();
		var response = toJson(events);

		//then
		assertEquals(response, Simulations.twoVTwoTeamSeed100());
	}

}
