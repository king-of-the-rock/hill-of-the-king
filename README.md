# king-of-the-rock

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=king-of-the-rock_hill-of-the-king&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=king-of-the-rock_hill-of-the-king) [![Bugs](https://sonarcloud.io/api/project_badges/measure?project=king-of-the-rock_hill-of-the-king&metric=bugs)](https://sonarcloud.io/summary/new_code?id=king-of-the-rock_hill-of-the-king) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=king-of-the-rock_hill-of-the-king&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=king-of-the-rock_hill-of-the-king) [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=king-of-the-rock_hill-of-the-king&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=king-of-the-rock_hill-of-the-king)

## Welcome to our game!
We are developing a multiplayer strategy game in a similar vein to Battle Royale and The Hunger Games. It will, however, not strictly be a "Battle Royale" game such as PUBG or Fortnite, but rather a simulation of how each players stats and the game environments sway the outcome. This is still in very early stages, but you can see our inspirations as well as design decisions in the Wiki page [here](https://gitlab.com/king-of-the-rock/hill-of-the-king/-/wikis/home).

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=king-of-the-rock_hill-of-the-king)
